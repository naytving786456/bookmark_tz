from django.urls import path
from . import views
from .views import TestView,SearchResultsView


app_name = 'search_book'

urlpatterns = [
    path('',views.TestView.as_view(), name='test_view'),
    path('/search',views.SearchResultsView.as_view(), name="search_result")

]


