from django.shortcuts import render
from django.views.generic import TemplateView, ListView
from books.models import Book
from django.db.models import Q

# Create your views here.
class TestView(TemplateView):
    template_name = 'search/search_view.html'
    context_object_name = 'test'

class SearchResultsView(ListView):
    model = Book
    template_name = 'search/search_result.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        object_list= Book.objects.filter(
            Q(title__icontains=query) | Q(author__icontains=query)
        )
        return object_list
