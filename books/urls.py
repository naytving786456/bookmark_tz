from django.urls import path
from . import views
from .forms import CommentForm
from .models import Comment,Book
from django.http import request
from .views import BookDetailView,CommentView

app_name = 'books'

urlpatterns = [
    path('',views.BookListView.as_view(), name='book_list'),
    path('<slug:slug>/', views.BookDetailView.as_view(), name='book_detail'),
    path('<int:pk>/comment',views.CommentView.as_view(), name='book_comments'),
    path('<int:pk>/comments', views.CommentView.comment_delete, name='delete'),
    path('<int:pk>/comment_edit', views.CommentUpdate.as_view(),name='edit'),

]
