from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import ListView, DetailView, View, UpdateView, TemplateView
from django.views.generic.edit import FormMixin
from .models import Book, Comment
from .forms import CommentForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.db.models import Q


class BookListView(ListView):
    queryset = Book.objects.all()
    context_object_name = 'books'
    template_name = 'books/book_list.html'


class BookDetailView(DetailView):
    model = Book
    context_object_name = 'book'
    template_name = 'books/book_detail.html'
    book = model.objects.all()

    def get_context_data(self, **kwargs):
        comments = self.book.filter(book_comments__active=True)
        context = super().get_context_data(**kwargs)
        context['comments'] = comments
        context['form'] = CommentForm()
        return context


class CommentView(LoginRequiredMixin, View):
    model = Comment
    context_object_name = 'comments'
    form_class = CommentForm
    template_name = 'books/comment.html'
    login_url = 'login'
    redirect_field_name = 'redirect to'

    def get(self, request, pk, book_pk, *args, **kwargs):
        form = self.form_class
        book = get_object_or_404(Book, id=book_pk)
        comment = get_object_or_404(Comment, pk=pk)
        return render(request, 'books/includes/edit_comment.html', {'book': book, 'comment': comment, 'form': form})

    def post(self, request, pk, *args, **kwargs):
        form = CommentForm(data=request.POST)
        comment = None
        book = get_object_or_404(Book, id=pk)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.book = book
            comment.username = request.user
            comment.save()
        return render(request, self.template_name, {'book': book, 'form': form, 'comment': comment})

    def comment_delete(self, pk):
        comment = get_object_or_404(Comment, pk=pk)
        comment.delete()
        return redirect('dashboard')


class CommentUpdate(LoginRequiredMixin, UpdateView):
    model = Comment
    form_class = CommentForm
    template_name = 'books/includes/edit_comment.html'
    login_url = 'login'
    redirect_field_name = 'redirect to'


