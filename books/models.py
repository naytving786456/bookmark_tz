from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse



class Book(models.Model):

    title = models.CharField(max_length=200)
    author = models.CharField(max_length=100)
    slug = models.SlugField(max_length=250,
                           unique_for_date='created')
    image = models.ImageField(upload_to='images/%Y/%m/%d/')
    description = models.TextField(blank=True)
    created = models.DateField(auto_now=True)

    class Meta:
        indexes = [
            models.Index(fields=['id','-title']),
        ]
        ordering = ['-title']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('books:book_detail',
                       args=[
                            self.slug]
                       )

class Comment(models.Model):
    book = models.ForeignKey(Book,
                             on_delete=models.CASCADE,
                             related_name='book_comments')
    username = models.ForeignKey(User,
                                 on_delete=models.CASCADE,
                                 related_name='user_comments')
    created=models.DateField(auto_now_add=True)
    body_description = models.TextField()
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ['created']
        indexes = [
            models.Index(fields=['id','created'])
        ]

    def __str__(self):
        return f'Комментирует {self.username} на {self.book}'

    def get_absolute_url(self):
        return reverse('book:book_detail', args=[str(self.book.slug)])