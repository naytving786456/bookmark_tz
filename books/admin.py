from django.contrib import admin
from .models import  Book,Comment

@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ['title','slug', 'image','created','description','author']
    list_filter = ['title','created']
    search_fields = ['title','author']

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ['username','body_description','created','active']
    list_filter = ['active', 'created', 'updated']
    search_fields = ['username','created','body_description']