from django.urls import path,include
from .views import RegistrationView
from . import views



urlpatterns=[

    path('', include('django.contrib.auth.urls')),
    path('', views.dashboard, name='dashboard'),
    path('register/',RegistrationView.as_view() , name='register'),
]