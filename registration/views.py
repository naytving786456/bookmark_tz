from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import View
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from .forms import LoginForm, UserRegistrationForm
from books.models import Book

class LoginView(View):
    template_name = 'account/login.html'
    form_class = LoginForm

    def get(self,request):
        form = self.form_class()
        return render(request, self.template_name,context={'form':form})

    def login(self,request):
        form = self.form_class(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(request,
                                username=cd['username'],
                                password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponse('Аутентификация успешна')
                else:
                    return HttpResponse('Отключенный аккаунт')
            else:
                return HttpResponse('Не правильный логин')
        else:
            form = self.form_class()
        return render(request,self.template_name,context={'form':form})


def dashboard(request):
    books = Book.objects.all()
    return render(request,
                'books/book_list.html',
                {'books': books})

class RegistrationView(View):
    template_name='account/register.html'
    template_name_done='account/register_done.html'
    form_class = UserRegistrationForm

    def get(self,request):
        user_form = self.form_class()
        return render(request,self.template_name,context={'user_form': user_form})

    def post(self,request):
        user_form=self.form_class(request.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=False)

            new_user.set_password(
                user_form.cleaned_data['password'])

            new_user.save()

            return render(request,self.template_name_done,context={'new_user': new_user})
        else:
            user_form=self.form_class()
            return render(request,self.template_name,context={'user_form': user_form})

