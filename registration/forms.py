from django import forms
from django.contrib.auth.models import User

class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

class UserRegistrationForm(forms.ModelForm):
    password = forms.CharField(label='Пароль',
                               widget=forms.PasswordInput)
    password_repeat = forms.CharField(label='Повторите пароль',
                                      widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username','first_name','email']

    def clean_password_repeat(self):
        cd = self.cleaned_data
        if cd['password'] != cd['password_repeat']:
            raise forms.ValidationError('Пароли не совпадают')
        return cd['password_repeat']